<?php


$numero = '11';

echo calcular($numero);

function calcular($n)
{
    if(!is_numeric($n)){
        return "Error: solo se permiten numeros ";    
    }

    if($n<0 or $n>11){
        return "Error: solo nummero de rango 1 a 11 ";
    }

    return fibonacci($n) + fibonacci($n +1);
}

function fibonacci($value)
{
    if(($value == 0  || $value == 1 )){
        return $value;
    } 

    $fibonacci = fibonacci($value - 1) + fibonacci($value - 2); 
    
    return $fibonacci;
}


?>